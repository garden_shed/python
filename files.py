#!/usr/sbin/python3
"""
Purpose: Ask for a filename, write soem stuff to it then push it out to screen
"""
fn = 'H';
print(chr(27) + "[2J");
while fn == 'H':
	fn = input("Enter the name of the file (H for help): ");
	if fn == 'H':
		print (__doc__)

with open(fn, "w+") as fh:
	insidebits = "Name of the file is:" + str(fh.name) + \
"\nFile mode: " + str(fh.mode) + \
"\nFile closed?: " + str(fh.closed);
	fh.write(insidebits);
	# now read it
	fh.seek(0);
	print ("line: ", fh.tell());
	for line in fh:
		print (line);
