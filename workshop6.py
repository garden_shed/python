#!/usr/sbin/python3
"""
Purpose: functions using lists, queries and inputs via menu interface
"""
__author__ = "Kristian Sotiroff"
import os, re; 
from sotirk import print_array;
from sotirk import flatten;

# your stuff below here
	
# display the menu
def drawMenu():
	os.system('clear');
	print ("Please choose an option to continue:");
	print ("(1) Enter details");
	print ("(2) Display all details");
	print ("(3) Search");
	print ("(q) Quit");
	choice = '';
	choice = input("so..?");
	return (choice);

# input
def inputDetails(array, fields):
	"""
	function: take an array and append a variable list of fields to it
	input: array to append to, *fields variable list of fields 
	output: null
	""" 	
	thisarray = {};
	for field in fields:
		label = field.title();
		thisinput = input (label + ": ");
		thisarray[field] = thisinput.strip();

	array.append(thisarray);
	return;

# display
def displayDetails(array, fieldorder):
	# print the colum headers once
	for field in fieldorder:
		print ("{0:20s}".format(field.capitalize()), end="");

	print ("\n");
	# now print the values
	for row in array:
		for field in fieldorder:
			print ("{0:20s}".format(row[field].capitalize()), end="");
		print ("\n");
	return;

# search
def searchDetails(needle, haystack, field='surname'):
	found = False;
	while not found:
		for thisrow in haystack:
			if re.match(needle, thisrow[field], re.I): # case insesitve search
				found = True;
				print ("I found this row:");
				print_array(thisrow);
		if found == False:
			print ("Sorry Dave, I cannot find ",needle);
			break;
	return;
	
def selector(choice, fields):
	print_array(fields);
	if choice == "1":
		inputDetails(allusers, fields);	
	elif choice == "2":
		displayDetails(allusers, fields);	
		input("Press any key to continue..");
	elif choice == "3":
		print ("Enter a field to search on from this list:", fields);
		field = input();
		while field not in fields:
			print ("Stop screwing around!! "+field+" is not in ", fields);
			field = input();
		what = input("Now enter a value to search for:");
		searchDetails(what.strip(), allusers, field.strip());
		input("Press any key to continue..");
	else:
		print (choice," is not an option...");
		input("Press any key to continue..");

	return;

#######################

allusers = [];
filename = 'users.txt';
fields = ('firstname', 'surname', 'age'); 
with open(filename, 'r') as fhandle:
	for line in fhandle:
		data = re.split("[ ]+", line, len(fields)); # only split for as many fields as we have
		thisrow = {};
		for i in range(len(fields)):
			field = fields[i].strip();
			thisrow[field] = data[i].strip();
		allusers.append(thisrow);

while True:
	choice = drawMenu();
	if choice == "q": break;
	selector(choice, fields);
print ("Bye...");
