"""
Purpose: module for useful things
"""
__author__ = "Kristian Sotiroff"

# your stuff below here
def print_array(var, start="", end=" "):
	if type(var) == type('a') or type(var) == type('1'):
		print (var);
	elif type(var) == type([]):
		k=0;
		for v in var:
			print (start+"{0} =>".format(k), end=end);
			if type(v) == type({}) or type(v) == type([]):
				print("[ ");
				print_array(v, start=start+" ",end=end+" ");
				print(start+"] ");
			else:
				print (v);
			k += 1;
	elif type(var) == type({}):
		for k,v in var.items():
			print(start+"{0} =>".format(k), end=end);
			if type(v) == type({}) or type(v) == type([]):
				print("[ ");
				print_array(v, start=start+" ",end=end+" ");
				print(start+"] ");
			else:
				print(v);

def flatten(array):
	first = True;
	for item in array:
		if first: flat = str(item); first = False;
		else: flat =  flat + "," + str(item);
	return flat;
 
