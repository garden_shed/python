#!/usr/sbin/python3
"""
Purpose : Generate a header for files
"""
__author__ = "Kristian Sotiroff"

import os;
os.system('clear');

# your stuff below here
fname = input ("What is the name of the file? ");
purpose = input ("What is the purpose of this file? ");
everything = [ 
"#!/usr/sbin/python3", 
'"""', 
'Purpose: '+ purpose, 
'"""', 
'__author__ = "Kristian Sotiroff"', 
"import os;", 
"#os.system('clear');", 
"from sotirk import print_array;",
"", 
"# your stuff below here", 
""
];

with open(fname, "w+") as fhandle:
	for line in everything:
		fhandle.write(line + "\n");	

os.system('chmod +x ' + fname);
print ("Have written out %s and made executable. Enjoy!" %(fname));
