#!/usr/sbin/python3
"""
Open a text file for reading, and print out requested line
"""
__author__ = "Kristian Sotiroff"

import os;
os.system('clear');

fname = 'test.txt';
lnum = 0;

with open(fname, "r+") as fhandle:
	lines = fhandle.readlines();

while True:
	lnum = input("what line of the file do you want to see (q to quit)? ");
	if lnum == 'q': break;
	os.system('clear');
	fsize = len(lines) - 1;
	if int(lnum) > fsize:
		print ("%s is too big. Try a number less than %s" %(lnum, str(fsize)));
	else:
		print ("Line number %s : %s " %(lnum, lines[int(lnum)]));

