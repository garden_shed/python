#!/usr/sbin/python3
"""
Purpose: conversion script
"""
__author__ = "Kristian Sotiroff"
import os;
import math;
#os.system('clear');
from sotirk import print_array;

# your stuff below hereinpower = 1;

Pi = input("Input power (W)?");
Po = input("Outout power (W)?");
Pi = int(Pi);
Po = int(Po);
gain = 10 * math.log10(Po / Pi);

print ("An input of %d and output of %d gives a gain of %d" %(Pi, Po, gain));


