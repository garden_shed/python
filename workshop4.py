#!/usr/sbin/python3
"""
Purpose: functions using lists, queries and inputs via menu interface
"""
__author__ = "Kristian Sotiroff"
import os;
import sotirk;
#os.system('clear');

# your stuff below here
allusers = [];
	
# display the menu
def drawMenu():
	os.system('clear');
	print ("Please choose an option to continue:");
	print ("(1) Enter details");
	print ("(2) Display all details");
	print ("(3) Search for Surname");
	print ("(q) Quit");
	choice = '';
	choice = input("so..?");
	return (choice);

# input
def inputDetails(array, *fields):
	"""
	function: take an array and append a variable list of fields to it
	input: array to append to, *fields variable list of fields 
	output: null
	""" 	
	thisarray = {};
	for field in fields:
		label = field.title();
		thisinput = input (label + ": ");
		thisarray[field] = thisinput.strip();

	array.append(thisarray);
	return;

# display
def displayDetails(array, *fieldorder):
	# print the colum headers once
	for field in fieldorder:
		print ("{0:20s}".format(field.capitalize()), end="");

	print ("\n");
	# now print the values
	for row in array:
		for field in fieldorder:
			print ("{0:20s}".format(row[field].capitalize()), end="");
		print ("\n");
	return;

# search
def searchDetails(needle, haystack, field='surname'):
	found = False;
	while not found:
		for thisrow in haystack:
			if thisrow[field] == needle:
				found = True;
				print ("I found this row:", thisrow);
	if found == False: print ("Sorry Dave, I cannot find ",needle);
	
def selector(choice, fields):
	if choice == "1":
		inputDetails(allusers, 'firstname', 'lastname', 'age');	
	elif choice == "2":
		displayDetails(allusers, 'firstname', 'lastname', 'age');	
		input("Press any key to continue..");
	elif choice == "3":
		print ("Enter a field to search on from this list:", fields);
		field = input();
		what = input("Now enter a value to search for:");
		searchDetails(what.strip(), allusers, field.strip());
		input("Press any key to continue..");
	else:
		print (choice," is not an option...");
		input("Press any key to continue..");

	return;


fields = ('firstname', 'lastname', 'age'); 
while True:
	choice = drawMenu();
	if choice == "q": break;
	selector(choice, fields);

# debug
sotirk.print_array(allusers);

print ("Bye...");
