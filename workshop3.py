#!/usr/sbin/python3
"""
Purpose: loops and searching
"""
__author__ = "Kristian Sotiroff"
import os;
os.system('clear');

# your stuff below here
finished = 'n';
allusers = [];

# input
print("Enter in user details, 'enter' to quit and display all");
while True:
	fname = input ("Firstname: ");
	fname = fname.strip();
	if fname == '': break;
	sname = input ("Surname: ");
	age = input ("Age: ");
	users = {"Firstname": fname, "Surname": sname.strip(), "Age": age.strip()};
	allusers.append(users);

# display
columndone = False;
for thisuser in allusers:
	# only print the colum headers once
	if columndone is False:
		column1, column2, column3 = thisuser.keys();
		print ("{0:10s} {1:10s} {2:10s}\n".format(column1, column2, column3));
		columndone = True;

	# now print the values
	print ("{0:10s} {1:10s} {2:10s}\n".format(thisuser['Firstname'].capitalize(), thisuser['Surname'].capitalize(), thisuser['Age']));

# search
while True:
	userfound = False;
	ssname = input("Enter a surname to search for (enter to quit):");
	ssname = ssname.strip();
	if ssname == '': break;
	for thisuser in allusers:
		if thisuser['Surname'] == ssname:
			userfound = True;
			print ("I found %s %s aged %s" %(thisuser['Firstname'].capitalize(), thisuser['Surname'].capitalize(), thisuser['Age']));
		if userfound: break;
	if userfound == False: print ("Sorry Dave, I cannot do that...");
	



print ("Bye...");
