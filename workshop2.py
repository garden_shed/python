#!/usr/sbin/python3
"""
Purpose: Working with tuples, list and dicts oh my!
"""
__author__ = "Kristian Sotiroff"
import os;
os.system('clear');

# your stuff below here
fname = input ("whats your first name Kristian?");
sname = input ("Whats your surname Sotiroff?");
age = input("How old are you, 5?");

mylist = [fname, sname, age];
mytuple = (fname, sname, age);
mydict = {'fname':fname, 'sname':sname, 'age':age};
 
print ("List\n");
f, s, a = mylist;
print ("%s %s %s" %(f, s, a));
print ("Tuple\n");
f, s, a = mytuple;
print ("%s %s %s" %(f, s, a));
print ("Dictionary\n");
for k,v in mydict.items():
	print ("%s = %s" %(k, v));
