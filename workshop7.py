#!/usr/sbin/python3
"""
Purpose: conversion script
"""
__author__ = "Kristian Sotiroff"
import os;
import math;
import re;
from sotirk import print_array;

# your stuff below hereinpower = 1;
os.system('clear');

class Gain:

	def __init__(self, Pi, Po):
		self.PowerInput = Pi;
		self.PowerOutput = Po;

	def calcGain(self):
		self.gain = 10 * math.log10(self.PowerOutput / self.PowerInput);	
	
	def showGain(self):
		print("The gain from %d W input and %d W output is %d Db" %(self.PowerInput, self.PowerOutput, self.gain));
	
class ezConvert:
	
	isCelcius = False;
	isFahren = False;
	unit = "?";

	def __init__(self, d):
		self.input = d;
		self.bits = re.match(r"(\d*)([cf])", self.input, re.I);
		self.value = int(self.bits.group(1));
		if self.bits.group(2).upper() == 'C':
			self.isCelcius = True;
			self.unit = 'F';
		elif self.bits.group(2).upper() == 'F':
			self.isFahren = True;
			self.unit = 'C';
		else:
			print("%s is incorrect. Please supply a unit (C or F) after the value" %(d));

	def calculate(self):
		if self.isCelcius:
			self.output = str((self.value / 5 * 9) + 32) + self.unit;
		elif self.isFahren:
			self.output = str((self.value - 32) / 9 * 5) + self.unit;
		else:
			print ("cannot calculate value with incorrect data..GiGo.");

	def showValue(self):
		print("%s is %s" %(self.input, self.output));

for unit in ['C', 'F']:
	for val in range(0,150,25):
		value = str(val)+unit;
		temp = ezConvert(value);
		temp.calculate();
		temp.showValue();


